-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql-sae-s5-codef.alwaysdata.net
-- Generation Time: Nov 14, 2023 at 03:34 PM
-- Server version: 10.6.14-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sae-s5-codef_fast-notes`
--
CREATE DATABASE IF NOT EXISTS `sae-s5-codef_fast-notes` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sae-s5-codef_fast-notes`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `code` varchar(255) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`code`, `isAdmin`) VALUES
('admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coefficient_ue`
--

CREATE TABLE `coefficient_ue` (
  `code_ressource` varchar(255) NOT NULL,
  `code_ue` varchar(255) NOT NULL,
  `coefficient` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coefficient_ue`
--

INSERT INTO `coefficient_ue` (`code_ressource`, `code_ue`, `coefficient`) VALUES
('BFTA5R01', 'BFTA51AU', 0.02),
('BFTA5R02', 'BFTA51AU', 0.09),
('BFTA5R03', 'BFTA51AU', 0.02),
('BFTA5R04', 'BFTA51AU', 0.06),
('BFTA5R05', 'BFTA51AU', 0.07),
('BFTA5R06', 'BFTA51AU', 0.07),
('BFTA5R07', 'BFTA51AU', 0.12),
('BFTA5R10', 'BFTA51AU', 0.02),
('BFTA5R11', 'BFTA51AU', 0.03),
('BFTA5S02', 'BFTA51AU', 0.15),
('BFTA5S04', 'BFTA51AU', 0.35),
('BFTM5S01', 'BFTA51AU', 0.35),
('BFTA5R01', 'BFTA52AU', 0.06),
('BFTA5R02', 'BFTA52AU', 0.07),
('BFTA5R03', 'BFTA52AU', 0.02),
('BFTA5R05', 'BFTA52AU', 0.05),
('BFTA5R06', 'BFTA52AU', 0.02),
('BFTA5R07', 'BFTA52AU', 0.40),
('BFTA5R08', 'BFTA52AU', 0.07),
('BFTA5R09', 'BFTA52AU', 0.13),
('BFTA5R11', 'BFTA52AU', 0.04),
('BFTA5S02', 'BFTA52AU', 0.15),
('BFTA5S04', 'BFTA52AU', 0.35),
('BFTM5S01', 'BFTA52AU', 0.35),
('BFTA5R03', 'BFTA56AU', 0.02),
('BFTA5R04', 'BFTA56AU', 0.02),
('BFTA5R10', 'BFTA56AU', 0.05),
('BFTA5R11', 'BFTA56AU', 0.13),
('BFTA5S02', 'BFTA56AU', 0.15),
('BFTA5S04', 'BFTA56AU', 0.35),
('BFTM5R01', 'BFTA56AU', 0.09),
('BFTM5R02', 'BFTA56AU', 0.06),
('BFTM5R03', 'BFTA56AU', 0.13),
('BFTM5S01', 'BFTA56AU', 0.35),
('BFTB5R01', 'BFTB51AU', 0.10),
('BFTB5R02', 'BFTB51AU', 0.12),
('BFTB5R03', 'BFTB51AU', 0.10),
('BFTB5R04', 'BFTB51AU', 0.10),
('BFTB5R09', 'BFTB51AU', 0.08),
('BFTB5S02', 'BFTB51AU', 0.15),
('BFTB5S04', 'BFTB51AU', 0.35),
('BFTM5S01', 'BFTB51AU', 0.35),
('BFTB5R01', 'BFTB53AU', 0.02),
('BFTB5R03', 'BFTB53AU', 0.10),
('BFTB5R04', 'BFTB53AU', 0.04),
('BFTB5R05', 'BFTB53AU', 0.13),
('BFTB5R06', 'BFTB53AU', 0.10),
('BFTB5R07', 'BFTB53AU', 0.07),
('BFTB5R08', 'BFTB53AU', 0.02),
('BFTB5R09', 'BFTB53AU', 0.02),
('BFTB5S02', 'BFTB53AU', 0.15),
('BFTB5S04', 'BFTB53AU', 0.35),
('BFTM5S01', 'BFTB53AU', 0.35),
('BFTB5R02', 'BFTB56AU', 0.02),
('BFTB5R08', 'BFTB56AU', 0.06),
('BFTB5R09', 'BFTB56AU', 0.12),
('BFTB5S02', 'BFTB56AU', 0.15),
('BFTB5S04', 'BFTB56AU', 0.35),
('BFTM5R01', 'BFTB56AU', 0.08),
('BFTM5R02', 'BFTB56AU', 0.06),
('BFTM5R03', 'BFTB56AU', 0.16),
('BFTM5S01', 'BFTB56AU', 0.35),
('BFTC5R01', 'BFTC54AU', 0.04),
('BFTC5R02', 'BFTC54AU', 0.09),
('BFTC5R03', 'BFTC54AU', 0.07),
('BFTC5R04', 'BFTC54AU', 0.06),
('BFTC5R05', 'BFTC54AU', 0.13),
('BFTC5R06', 'BFTC54AU', 0.07),
('BFTC5R08', 'BFTC54AU', 0.02),
('BFTC5R09', 'BFTC54AU', 0.02),
('BFTC5S02', 'BFTC54AU', 0.15),
('BFTC5S04', 'BFTC54AU', 0.35),
('BFTM5S01', 'BFTC54AU', 0.35),
('BFTC5R01', 'BFTC55AU', 0.04),
('BFTC5R02', 'BFTC55AU', 0.12),
('BFTC5R08', 'BFTC55AU', 0.26),
('BFTC5R09', 'BFTC55AU', 0.08),
('BFTC5S02', 'BFTC55AU', 0.15),
('BFTC5S04', 'BFTC55AU', 0.35),
('BFTM5S01', 'BFTC55AU', 0.35),
('BFTC5R07', 'BFTC56AU', 0.07),
('BFTC5R08', 'BFTC56AU', 0.04),
('BFTC5R09', 'BFTC56AU', 0.10),
('BFTC5S02', 'BFTC56AU', 0.15),
('BFTC5S04', 'BFTC56AU', 0.35),
('BFTM5R01', 'BFTC56AU', 0.08),
('BFTM5R02', 'BFTC56AU', 0.06),
('BFTM5R03', 'BFTC56AU', 0.15),
('BFTM5S01', 'BFTC56AU', 0.35);

-- --------------------------------------------------------

--
-- Table structure for table `competences`
--

CREATE TABLE `competences` (
  `code` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competences`
--

INSERT INTO `competences` (`code`, `libelle`) VALUES
('B3FTA1AB', 'BLCC1 REALISER NIV3'),
('B3FTA2AB', 'BLCC1 OPTIMISER NIV3'),
('B3FTA3AB', 'BLCC1 ADMINISTRER NIV3'),
('B3FTA4AB', 'BLCC1 GERER NIV3'),
('B3FTA5AB', 'BLCC1 CONDUIRE NIV3'),
('B3FTA6AB', 'BLCC1 COLLABORER NIV3');

-- --------------------------------------------------------

--
-- Table structure for table `eleves`
--

CREATE TABLE `eleves` (
  `code` varchar(255) NOT NULL,
  `identification` varchar(255) NOT NULL,
  `id_groupe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eleves`
--

INSERT INTO `eleves` (`code`, `identification`, `id_groupe`) VALUES
('eleveA', 'eleveA', 'inS5_A'),
('eleveA2', 'eleveA2', 'inS5_A'),
('eleveB', 'eleveB', 'inS5_B'),
('eleveB2', 'eleveB2', 'inS5_B'),
('eleveC', 'eleveC', 'inS5_C'),
('eleveC2', 'eleveC2', 'inS5_C');

-- --------------------------------------------------------

--
-- Table structure for table `enseignements`
--

CREATE TABLE `enseignements` (
  `code_prof` varchar(255) NOT NULL,
  `id_groupe` varchar(255) NOT NULL,
  `code_ressource` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enseignements`
--

INSERT INTO `enseignements` (`code_prof`, `id_groupe`, `code_ressource`) VALUES
('prof', 'inS5_A', 'BFTA5R01'),
('prof', 'inS5_B', 'BFTA5R01'),
('prof', 'inS5_B', 'BFTA5R08');

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `coefficient` double(8,2) NOT NULL,
  `type` varchar(255) NOT NULL,
  `date_epreuve` timestamp NULL DEFAULT NULL,
  `date_rattrapage` timestamp NULL DEFAULT NULL,
  `code_ressource` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `evaluations`
--

INSERT INTO `evaluations` (`id`, `libelle`, `coefficient`, `type`, `date_epreuve`, `date_rattrapage`, `code_ressource`) VALUES
(572, 'R5.C04-1 WEB BD', 0.60, 'QCM', NULL, NULL, 'BFTC5R01'),
(573, 'R5.C04-2 WEB BD', 0.40, 'Projet', NULL, NULL, 'BFTC5R01'),
(574, 'R5.C05-1 NOUVELLES BD', 0.60, 'Ecrit', NULL, NULL, 'BFTC5R02'),
(575, 'R5.C05-2 NOUVELLES BD', 0.40, 'Travaux Pratiques', NULL, NULL, 'BFTC5R02'),
(576, 'R5.C06-1 EXPLOITATION BD', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R03'),
(577, 'R5.C06-2 EXPLOITATION BD', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R03'),
(578, 'R5.C07-1 DONNÉES MASSIVES', 1.00, 'Ecrit', NULL, NULL, 'BFTC5R04'),
(579, 'R5.C08-1 TECHNIQUES D\'IA', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R05'),
(580, 'R5.C08-2 TECHNIQUES D\'IA', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R05'),
(581, 'R5.C09-1 STAT INFÉRENTIELLE', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R06'),
(582, 'R5.C09-2 STAT INFÉRENTIELLE', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R06'),
(583, 'R5.C10-1 ECO DURABLE ET NUM', 1.00, 'Ecrit', NULL, NULL, 'BFTC5R07'),
(584, 'R5.C11-1 OPTIMIS DONNÉES SI', 1.00, 'Ecrit', NULL, NULL, 'BFTC5R08'),
(585, 'R5.C12-1 ANGLAIS', 0.50, 'Ecrit', NULL, NULL, 'BFTC5R09'),
(586, 'R5.C12-2 ANGLAIS', 0.50, 'oral', NULL, NULL, 'BFTC5R09'),
(587, 'R5.01-1 INIT. MANAGEMENT', 1.00, 'Ecrit', NULL, NULL, 'BFTM5R01'),
(591, 'S5.C01P-1 DATAMINING 1', 1.00, 'Projet', NULL, NULL, 'BFTC5S02'),
(592, 'S5.C02P-1 DATAMINING 2', 1.00, 'Projet', NULL, NULL, 'BFTC5S04'),
(594, 'R5.B04-1 PROG. SYSTÈME', 0.50, 'Écrit', NULL, NULL, 'BFTB5R01'),
(595, 'R5.B04-2 PROG. SYSTÈME', 0.50, 'TP', NULL, NULL, 'BFTB5R01'),
(596, 'R5.B05-1 AUTOMATISATION', 1.00, 'QCM', NULL, NULL, 'BFTB5R02'),
(597, 'R5.B06-1 SERVICES COMPLEXES', 0.50, 'Ecrit', NULL, NULL, 'BFTB5R03'),
(598, 'R5.B06-2 SERVICES COMPLEXES', 0.50, 'Travaux Pratiques', NULL, NULL, 'BFTB5R03'),
(599, 'R5.B07-1 VIRTUALIS. AVANCÉE', 0.60, 'Ecrit', NULL, NULL, 'BFTB5R04'),
(600, 'R5.B07-2 VIRTUALIS. AVANCÉE', 0.40, 'Travaux Pratiques', NULL, NULL, 'BFTB5R04'),
(601, 'R5.B08-1 CONTINUITÉ SERVICE', 0.60, 'Écrit', NULL, NULL, 'BFTB5R05'),
(602, 'R5.B08-2 CONTINUITÉ SERVICE', 0.40, 'TP', NULL, NULL, 'BFTB5R05'),
(603, 'R5.B09-1 CYBERSÉCURITÉ', 0.50, 'Ecrit', NULL, NULL, 'BFTB5R06'),
(604, 'R5.B09-2 CYBERSÉCURITÉ', 0.50, 'Travaux pratiques', NULL, NULL, 'BFTB5R06'),
(605, 'R5.B10-1 MODÉLISATIONS MATH', 0.50, 'MEM', NULL, NULL, 'BFTB5R07'),
(606, 'R5.B10-2 MODÉLISATIONS MATH', 0.50, 'SOU', NULL, NULL, 'BFTB5R07'),
(607, 'R5.B11-1 ECO DURABLE ET NUM', 1.00, 'Ecrit', NULL, NULL, 'BFTB5R08'),
(608, 'R5.B12-1 ANGLAIS', 0.50, 'Ecrit', NULL, NULL, 'BFTB5R09'),
(609, 'R5.B12-2 ANGLAIS', 0.50, 'oral', NULL, NULL, 'BFTB5R09'),
(614, 'S5.B01P-1 EVO INFRASTRUCT 1', 1.00, 'Projet', NULL, NULL, 'BFTB5S02'),
(615, 'S5.B02P-1 EVO INFRASTRUCT 2', 1.00, 'Projet', NULL, NULL, 'BFTB5S04'),
(617, 'R5.A04-1 QUALITÉ ALGO', 1.00, 'Ecrit', NULL, NULL, 'BFTA5R01'),
(618, 'R5.A05-1 PROG. AVANCÉE', 0.50, 'Travaux Pratiques', NULL, NULL, 'BFTA5R02'),
(619, 'R5.A05-2 PROG. AVANCÉE', 0.50, 'Travaux Pratiques', NULL, NULL, 'BFTA5R02'),
(620, 'R5.A06-1 PROG. MULTIMÉDIA', 1.00, 'Travaux pratiques', NULL, NULL, 'BFTA5R03'),
(621, 'R5.A07-1 AUTOMATISATION', 1.00, 'QCM', NULL, NULL, 'BFTA5R04'),
(622, 'R5.A08-1 QUALITÉ DÉVELOP.', 0.50, 'Travaux pratiques', NULL, NULL, 'BFTA5R05'),
(623, 'R5.A08-2 QUALITÉ DÉVELOP.', 0.50, 'Travaux Pratiques', NULL, NULL, 'BFTA5R05'),
(624, 'R5.A09-1 VIRTUALIS. AVANCÉE', 0.60, 'Ecrit', NULL, NULL, 'BFTA5R06'),
(625, 'R5.A09-2 VIRTUALIS. AVANCÉE', 0.40, 'Travaux Pratiques', NULL, NULL, 'BFTA5R06'),
(626, 'R5.A10-1 NOUVELLES BD', 0.60, 'Ecrit', NULL, NULL, 'BFTA5R07'),
(627, 'R5.A10-2 NOUVELLES BD', 0.40, 'Travaux Pratiques', NULL, NULL, 'BFTA5R07'),
(628, 'R5.A11-1 AIDE À LA DÉCISION', 1.00, 'ECR', NULL, NULL, 'BFTA5R08'),
(629, 'R5.A12-1 MODÉLISATIONS MATH', 0.50, 'MEM', NULL, NULL, 'BFTA5R09'),
(630, 'R5.A12-2 MODÉLISATIONS MATH', 0.50, 'SOU', NULL, NULL, 'BFTA5R09'),
(631, 'R5.A13-1 ECO DURABLE ET NUM', 1.00, 'Ecrit', NULL, NULL, 'BFTA5R10'),
(632, 'R5.A14-1 ANGLAIS', 0.50, 'Ecrit', NULL, NULL, 'BFTA5R11'),
(633, 'R5.A14-2 ANGLAIS', 0.50, 'oral', NULL, NULL, 'BFTA5R11'),
(635, 'R5.02-1 PPP', 1.00, 'Oral', NULL, NULL, 'BFTM5R02'),
(636, 'R5.03-1 COMMUNICATION', 0.50, 'Oral', NULL, NULL, 'BFTM5R03'),
(637, 'R5.03-2 COMMUNICATION', 0.50, 'Ecrit', NULL, NULL, 'BFTM5R03'),
(638, 'S5.A01P-1 DÉV. AVANCÉ 1', 1.00, 'Projet', NULL, NULL, 'BFTA5S02'),
(639, 'S5.A02P-1 DÉV. AVANCÉ 2', 1.00, 'Projet', NULL, NULL, 'BFTA5S04'),
(640, 'S5.St-1 STAGE', 1.00, 'Compte-rendu', NULL, NULL, 'BFTM5S01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groupes`
--

CREATE TABLE `groupes` (
  `id` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `annee` year(4) NOT NULL,
  `parcours` varchar(255) NOT NULL,
  `id_semestre` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groupes`
--

INSERT INTO `groupes` (`id`, `libelle`, `annee`, `parcours`, `id_semestre`) VALUES
('inS5_A', 'A', '2023', 'RAPP', 5),
('inS5_B', 'B', '2023', 'RAPP', 5),
('inS5_C', 'C', '2023', 'DACS', 5),
('inS5_D', 'D', '2023', 'AGED', 5);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(4, '2023_10_17_073802_create_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `note_evaluation`
--

CREATE TABLE `note_evaluation` (
  `note` varchar(255) NOT NULL,
  `id_evaluation` bigint(20) UNSIGNED NOT NULL,
  `code_eleve` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `note_evaluation`
--

INSERT INTO `note_evaluation` (`note`, `id_evaluation`, `code_eleve`) VALUES
('19', 587, 'eleveA'),
('1.5', 587, 'eleveA2'),
('4.5', 587, 'eleveB'),
('8', 617, 'eleveA'),
('20', 617, 'eleveA2'),
('5', 617, 'eleveB'),
('7', 617, 'eleveB2'),
('4', 621, 'eleveA'),
('15', 622, 'eleveA'),
('1', 628, 'eleveB'),
('19', 628, 'eleveB2');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professeurs`
--

CREATE TABLE `professeurs` (
  `code` varchar(255) NOT NULL,
  `isProf` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `professeurs`
--

INSERT INTO `professeurs` (`code`, `isProf`) VALUES
('prof', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ressources`
--

CREATE TABLE `ressources` (
  `code` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ressources`
--

INSERT INTO `ressources` (`code`, `libelle`) VALUES
('BFTA5R01', 'R5.A04 - Qualité algorithmique'),
('BFTA5R02', 'R5.A05 - Programmation avancée'),
('BFTA5R03', 'R5.A06 - Programmation multimédia'),
('BFTA5R04', 'R5.A07 - Automatisation'),
('BFTA5R05', 'R5.A08 - Qualité de développement'),
('BFTA5R06', 'R5.A09 - Virtualisation avancée'),
('BFTA5R07', 'R5.A10 - Nouvelles bases de données'),
('BFTA5R08', 'R5.A11 - Aide à la décision'),
('BFTA5R09', 'R5.A12 - Modélisations mathématiques'),
('BFTA5R10', 'R5.A13 - Economie durable et numérique'),
('BFTA5R11', 'R5.A14 - Anglais'),
('BFTA5S01', 'S5.A01 - Développement avancé 1'),
('BFTA5S02', 'S5.A01.P - Développement avancé 1'),
('BFTA5S03', 'S5.A02 - Développement avancé 2'),
('BFTA5S04', 'S5.A02.P - Développement avancé 2'),
('BFTA5S05', 'P5.A01 - Démarche Portfolio'),
('BFTB5R01', 'R5.B04 - Programmation système'),
('BFTB5R02', 'R5.B05 - Automatisation'),
('BFTB5R03', 'R5.B06 - Services complexes'),
('BFTB5R04', 'R5.B07 - Virtualisation avancée'),
('BFTB5R05', 'R5.B08 - Continuité de service'),
('BFTB5R06', 'R5.B09 - Cybersécurité'),
('BFTB5R07', 'R5.B10 - Modélisations mathématiques'),
('BFTB5R08', 'R5.B11 - Economie durable et numérique'),
('BFTB5R09', 'R5.B12 - Anglais'),
('BFTB5S01', 'S5.B01 - Evolution d\'infrastructure 1'),
('BFTB5S02', 'S5.B01.P - Evolution d\'infrastructure 1'),
('BFTB5S03', 'S5.B02 - Evolution d\'infrastructure 2'),
('BFTB5S04', 'S5.B02.P - Evolution d\'infrastructure 2'),
('BFTB5S05', 'P5.B01 - Démarche Portfolio'),
('BFTC5R01', 'R5.C04 - Web bases de données'),
('BFTC5R02', 'R5.C05 - Nouvelles bases de données'),
('BFTC5R03', 'R5.C06 - Exploitation bases de données'),
('BFTC5R04', 'R5.C07 - Données massives'),
('BFTC5R05', 'R5.C08 - Techniques d\'intelligence artificielle'),
('BFTC5R06', 'R5.C09 - Statistique inférentielle'),
('BFTC5R07', 'R5.C10 - Economie durable et numérique'),
('BFTC5R08', 'R5.C11 - Optimisation données et systèmes décisionnels'),
('BFTC5R09', 'R5.C12 - Anglais'),
('BFTC5S01', 'S5.C01 - Datamining 1'),
('BFTC5S02', 'S5.C01.P - Datamining 1'),
('BFTC5S03', 'S5.C02 - Datamining 2'),
('BFTC5S04', 'S5.C02.P - Datamining 2'),
('BFTC5S05', 'P5.C01 - Démarche Portfolio'),
('BFTM5R01', 'R5.01 - Initiation au management'),
('BFTM5R02', 'R5.02 - Projet professionnel et personnel'),
('BFTM5R03', 'R5.03 - Communication'),
('BFTM5S01', 'S5.St - Stage');

-- --------------------------------------------------------

--
-- Table structure for table `ressource_groupe`
--

CREATE TABLE `ressource_groupe` (
  `id_groupe` varchar(255) NOT NULL,
  `code_ressource` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ressource_groupe`
--

INSERT INTO `ressource_groupe` (`id_groupe`, `code_ressource`) VALUES
('inS5_A', 'BFTA5R01'),
('inS5_A', 'BFTA5R02'),
('inS5_A', 'BFTA5R03'),
('inS5_A', 'BFTA5R04'),
('inS5_A', 'BFTA5R05'),
('inS5_A', 'BFTA5R06'),
('inS5_A', 'BFTA5R07'),
('inS5_A', 'BFTA5R08'),
('inS5_A', 'BFTA5R09'),
('inS5_A', 'BFTA5R10'),
('inS5_A', 'BFTA5R11'),
('inS5_A', 'BFTA5S01'),
('inS5_A', 'BFTA5S02'),
('inS5_A', 'BFTA5S03'),
('inS5_A', 'BFTA5S04'),
('inS5_A', 'BFTA5S05'),
('inS5_A', 'BFTM5R01'),
('inS5_A', 'BFTM5R02'),
('inS5_A', 'BFTM5R03'),
('inS5_A', 'BFTM5S01'),
('inS5_C', 'BFTB5R01'),
('inS5_C', 'BFTB5R02'),
('inS5_C', 'BFTB5R03'),
('inS5_C', 'BFTB5R04'),
('inS5_C', 'BFTB5R05'),
('inS5_C', 'BFTB5R06'),
('inS5_C', 'BFTB5R07'),
('inS5_C', 'BFTB5R08'),
('inS5_C', 'BFTB5R09'),
('inS5_C', 'BFTB5S01'),
('inS5_C', 'BFTB5S02'),
('inS5_C', 'BFTB5S03'),
('inS5_C', 'BFTB5S04'),
('inS5_C', 'BFTB5S05'),
('inS5_C', 'BFTM5R01'),
('inS5_C', 'BFTM5R02'),
('inS5_C', 'BFTM5R03'),
('inS5_C', 'BFTM5S01'),
('inS5_D', 'BFTC5R01'),
('inS5_D', 'BFTC5R02'),
('inS5_D', 'BFTC5R03'),
('inS5_D', 'BFTC5R04'),
('inS5_D', 'BFTC5R05'),
('inS5_D', 'BFTC5R06'),
('inS5_D', 'BFTC5R07'),
('inS5_D', 'BFTC5R08'),
('inS5_D', 'BFTC5R09'),
('inS5_D', 'BFTC5S01'),
('inS5_D', 'BFTC5S02'),
('inS5_D', 'BFTC5S03'),
('inS5_D', 'BFTC5S04'),
('inS5_D', 'BFTC5S05'),
('inS5_D', 'BFTM5R01'),
('inS5_D', 'BFTM5R02'),
('inS5_D', 'BFTM5R03'),
('inS5_D', 'BFTM5S01');

-- --------------------------------------------------------

--
-- Table structure for table `semestres`
--

CREATE TABLE `semestres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `semestres`
--

INSERT INTO `semestres` (`id`, `libelle`) VALUES
(5, 'Semestre 5');

-- --------------------------------------------------------

--
-- Table structure for table `ue`
--

CREATE TABLE `ue` (
  `code` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `code_competence` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ue`
--

INSERT INTO `ue` (`code`, `libelle`, `code_competence`) VALUES
('BFTA51AU', 'UE 5.1 Réaliser un développement', 'B3FTA1AB'),
('BFTA52AU', 'UE 5.2 Optimiser des applications', 'B3FTA2AB'),
('BFTA56AU', 'UE 5.6 Collaborer au sein d\'une équipe', 'B3FTA6AB'),
('BFTB51AU', 'UE 5.1 Réaliser un développement', 'B3FTA1AB'),
('BFTB53AU', 'UE 5.3 Administrer des systèmes', 'B3FTA3AB'),
('BFTB56AU', 'UE 5.6 Collaborer au sein d\'une équipe', 'B3FTA6AB'),
('BFTC54AU', 'UE 5.4 Gérer des données', 'B3FTA4AB'),
('BFTC55AU', 'UE 5.5 Conduire un projet', 'B3FTA5AB'),
('BFTC56AU', 'UE 5.6 Collaborer au sein d\'une équipe', 'B3FTA6AB');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `code` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`code`, `password`, `email`, `nom`, `prenom`) VALUES
('admin', '$2y$10$tm.HpxKJvYLAMwy.XdiYhejJCj/lEn3f/qM/QDySOQgKk/8i4osMi', 'admin@admin.com', 'nomAdmin', 'prenomAdmin'),
('eleveA', '$2y$10$YILAhVYgKYsbkBL85lXRVuvCpJXR8uNiBXm/x.sfNJnaC1V8OlVO2', 'eleveA@eleveA.com', 'nomEleveA', 'prenomEleveA'),
('eleveA2', '$2y$10$Xvf70heYuqEzjwGn8J3UgOCLEa2qwQ9k0AVvSh4BWJeqnJxbEpYky', 'eleveA2@eleveA2.com', 'nomEleveA2', 'prenomEleveA2'),
('eleveB', '$2y$10$Y8Cuv9nj4b.EOAXBL3.Ebu57pr.3lqxAD0nLKTLMeWuZ5bS1cx.O6', 'eleveB@eleveB.com', 'nomEleveB', 'prenomEleveB'),
('eleveB2', '$2y$10$g3U/ZULK2H1hshzYRUXxgu0rwmTw2dZ2j4/5.basr35ccXVm03U9a', 'eleveB2@eleveB2.com', 'nomEleveB2', 'prenomEleveB2'),
('eleveC', '$2y$10$d9q6GMBb3UrHM4E5geICU.Lj8GgIJxFdR3HwQniCuSalvgilOMnee', 'eleveC@eleveC.com', 'nomEleveC', 'prenomEleveC'),
('eleveC2', '$2y$10$pj87VY1ZGYGIl8kofArDDu5YCbezLLsaWk9Y9OzPxIdk.CCfcRPKS', 'eleveC2@eleveC2.com', 'nomEleveC2', 'prenomEleveC2'),
('prof', '$2y$10$LfUOcWfH4ksrKldYzxDO7.I6ErAdS5Be1H81ztoxRjuGBbBsdlMNq', 'prof@prof.com', 'nomProf', 'prenomProf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `coefficient_ue`
--
ALTER TABLE `coefficient_ue`
  ADD PRIMARY KEY (`code_ue`,`code_ressource`),
  ADD KEY `coefficient_ue_code_ressource_foreign` (`code_ressource`);

--
-- Indexes for table `competences`
--
ALTER TABLE `competences`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `eleves`
--
ALTER TABLE `eleves`
  ADD PRIMARY KEY (`code`),
  ADD KEY `eleves_id_groupe_foreign` (`id_groupe`);

--
-- Indexes for table `enseignements`
--
ALTER TABLE `enseignements`
  ADD PRIMARY KEY (`id_groupe`,`code_ressource`,`code_prof`),
  ADD KEY `enseignements_code_prof_foreign` (`code_prof`),
  ADD KEY `enseignements_code_ressource_foreign` (`code_ressource`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evaluations_code_ressource_foreign` (`code_ressource`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `groupes`
--
ALTER TABLE `groupes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupes_id_semestre_foreign` (`id_semestre`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `note_evaluation`
--
ALTER TABLE `note_evaluation`
  ADD PRIMARY KEY (`id_evaluation`,`code_eleve`),
  ADD KEY `note_evaluation_code_eleve_foreign` (`code_eleve`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `professeurs`
--
ALTER TABLE `professeurs`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `ressources`
--
ALTER TABLE `ressources`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `ressource_groupe`
--
ALTER TABLE `ressource_groupe`
  ADD PRIMARY KEY (`id_groupe`,`code_ressource`),
  ADD KEY `ressource_groupe_code_ressource_foreign` (`code_ressource`);

--
-- Indexes for table `semestres`
--
ALTER TABLE `semestres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ue`
--
ALTER TABLE `ue`
  ADD PRIMARY KEY (`code`),
  ADD KEY `ue_code_competence_foreign` (`code_competence`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=641;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_code_foreign` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `coefficient_ue`
--
ALTER TABLE `coefficient_ue`
  ADD CONSTRAINT `coefficient_ue_code_ressource_foreign` FOREIGN KEY (`code_ressource`) REFERENCES `ressources` (`code`),
  ADD CONSTRAINT `coefficient_ue_code_ue_foreign` FOREIGN KEY (`code_ue`) REFERENCES `ue` (`code`);

--
-- Constraints for table `eleves`
--
ALTER TABLE `eleves`
  ADD CONSTRAINT `eleves_code_foreign` FOREIGN KEY (`code`) REFERENCES `users` (`code`),
  ADD CONSTRAINT `eleves_id_groupe_foreign` FOREIGN KEY (`id_groupe`) REFERENCES `groupes` (`id`);

--
-- Constraints for table `enseignements`
--
ALTER TABLE `enseignements`
  ADD CONSTRAINT `enseignements_code_prof_foreign` FOREIGN KEY (`code_prof`) REFERENCES `professeurs` (`code`),
  ADD CONSTRAINT `enseignements_code_ressource_foreign` FOREIGN KEY (`code_ressource`) REFERENCES `ressources` (`code`),
  ADD CONSTRAINT `enseignements_id_groupe_foreign` FOREIGN KEY (`id_groupe`) REFERENCES `groupes` (`id`);

--
-- Constraints for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD CONSTRAINT `evaluations_code_ressource_foreign` FOREIGN KEY (`code_ressource`) REFERENCES `ressources` (`code`);

--
-- Constraints for table `groupes`
--
ALTER TABLE `groupes`
  ADD CONSTRAINT `groupes_id_semestre_foreign` FOREIGN KEY (`id_semestre`) REFERENCES `semestres` (`id`);

--
-- Constraints for table `note_evaluation`
--
ALTER TABLE `note_evaluation`
  ADD CONSTRAINT `note_evaluation_code_eleve_foreign` FOREIGN KEY (`code_eleve`) REFERENCES `eleves` (`code`),
  ADD CONSTRAINT `note_evaluation_id_evaluation_foreign` FOREIGN KEY (`id_evaluation`) REFERENCES `evaluations` (`id`);

--
-- Constraints for table `professeurs`
--
ALTER TABLE `professeurs`
  ADD CONSTRAINT `professeurs_code_foreign` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `ressource_groupe`
--
ALTER TABLE `ressource_groupe`
  ADD CONSTRAINT `ressource_groupe_code_ressource_foreign` FOREIGN KEY (`code_ressource`) REFERENCES `ressources` (`code`),
  ADD CONSTRAINT `ressource_groupe_id_groupe_foreign` FOREIGN KEY (`id_groupe`) REFERENCES `groupes` (`id`);

--
-- Constraints for table `ue`
--
ALTER TABLE `ue`
  ADD CONSTRAINT `ue_code_competence_foreign` FOREIGN KEY (`code_competence`) REFERENCES `competences` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
