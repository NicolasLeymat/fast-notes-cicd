FROM php:8.1.10-cli

WORKDIR /var/www/html

RUN apt-get update && \
    apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libjpeg62-turbo-dev \
    libfreetype-dev \
    libzip-dev

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd zip
RUN docker-php-ext-configure gd --with-freetype --with-jpeg

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY composer.json composer.lock ./

RUN composer install --no-scripts --no-autoloader

COPY . .

RUN composer dump-autoload && \
    chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache
RUN apt-get update -qy
RUN apt-get install -y unzip

EXPOSE 8000

CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
